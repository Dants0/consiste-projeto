package consisteprojetoimc;

import java.text.DecimalFormat;

public class IMC{
    DadosPessoa vet[];//vetor para pessoas
    int total;
    
    public IMC(int nPessoa){
        vet = new DadosPessoa[nPessoa];
        total=0;
    }
    
    
    
    public void adicionaPessoa(DadosPessoa obj){
        if(total<vet.length){
            vet[total]=obj;
            total++;
        }    
    }
    
    public void status(Float calcimc){
        int i;
        System.out.println("----------- Dados ------------");
        
        for(i=0;i<vet.length;i++){
            DecimalFormat num = new DecimalFormat("##.##");
            System.out.println(" " +vet[i].getnomePessoa().toUpperCase() + " " + vet[i].getsobrePessoa().toUpperCase() + " " + num.format(vet[i].getcalcimc()));
        }
    }
}
