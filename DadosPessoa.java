package consisteprojetoimc;

public class DadosPessoa{
    private String nomePessoa;
    private String sobrePessoa;
    private float altura,peso,calcimc;
    
    
    public DadosPessoa(String nomePessoa ,String sobrePessoa, float altura, float peso, float calcimc){
        
        this.nomePessoa=nomePessoa;
        this.sobrePessoa=sobrePessoa;
        this.altura=altura;
        this.peso=peso;
        this.calcimc=calcimc;
    }
    

    public void setnomePessoa(String nomePessoa){
        this.nomePessoa=nomePessoa;
    }    
    
    
    public String getnomePessoa(){
        return nomePessoa;
    }

   
    public void setsobrePessoa(String sobrePessoa){
        this.sobrePessoa=sobrePessoa;
    }    
    
    
    public String getsobrePessoa(){
        return sobrePessoa;
    }

    
    public void setaltura(Float altura){
        this.altura=altura;
    }    
    
    
    public float getaltura(){
        return altura;
    }

    public void setpeso(float peso){
        this.peso=peso;
    }    
    
    
    public float getpeso(){
        return peso;
    }
    
    public void setcalcimc(Float calcimc){
        this.calcimc=calcimc;
    }
    
    public float getcalcimc(){
        /*
        if((calcimc<18.5)){
            System.out.println("Abaixo do normal");
        }else if((calcimc>18.5)&&(calcimc<24.9)){
            System.out.println("Peso ideal");
        }else if((calcimc>25.0)&&(calcimc<29.9)){
            System.out.println("Excesso de peso");
        }else if((calcimc>30.0)&&(calcimc<34.9)){
            System.out.println("Obesidade I");
        }else if((calcimc>35.0)&&(calcimc<39.9)){
            System.out.println("Obesidade II");
        }else{
            System.out.println("Obesidade III");
        }
        */
        return calcimc;
    }

}
